import './ImageList.css';
import React from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';

import ImageCard from './ImageCard';
/*
const test_renderList = props => {
  const images = props.imgs.map(image => {
    return <ImageCard key={image.id} image={image} />;
  });

  return <div className="image-list">{images}</div>;
};
*/
class ImageList extends React.Component {

  renderList() {
    console.log(this.props)
    return this.props.imgs.map(image => {
      return <ImageCard key={image.id} image={image} />;
    });
  }

  render() {
    return <div className="image-list">{this.renderList()}</div>;
  }
}

const mapStateToProps = state => {
  console.log('ImageList mapState');
  console.log(state);
  return { imgs: state.imgs };
};

export default connect(
  mapStateToProps,
  { fetchPosts }
)(ImageList);

//export default ImageList;
